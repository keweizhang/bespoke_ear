import json
import pandas as pd 
from pandas import DataFrame, read_csv


def EAR(semi_annual_rate):
	'''
	calculate and return effective annual rate (EAR)
	Keyword arguments:
	semi_annual_rate: semi-annual rate
	return: effective annual rate
	'''
	EAR = (1 + (semi_annual_rate * (10 **-2))/2.0)**2 - 1
	return EAR


def dump_json_EAR(filename, sheetindex, parse_cols):
	'''
	dump json format EAR to EAR.json
	Keyword arguments:
	filename: original .xls file path
	sheetindex: the target sheet index
	parse_cols: the column indexs that represent date and semi-annal rate, in a list format e.g. [0,29] ([column of date, column of semi-annual rate]) 
	return json format EAR: [{"date":xxxx, "effective annual rate"}]
	'''
	df = pd.read_excel(filename, sheetname = sheetindex, names = ['date','semi-anual rate'], index_col = None, skiprows = 10, parse_cols = parse_cols)
	df = df.dropna(how = 'any')
	df = df.reset_index()
	del df['index']

	#calculate the effective annual rate
	date_list,rate_list = zip(*df.values)
	ear_list = map(lambda x: EAR(x), rate_list)
	df['effective annual rate'] = ear_list

	# return json format EAR
	export_df = df[['date','effective annual rate']]
	return export_df.to_json(orient = "records")

