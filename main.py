import json
import matplotlib.pyplot as plt
import pandas as pd 
from EAR_lib import EAR


file1 = r'raw_data/f16hist.xls'
file2 = r'raw_data/f16hist-2009-2015.xls'


if __name__ == "__main__":
	# load xls file to dataframe
	df1 = pd.read_excel(file1, sheetname = 0, names = ['date','semi-anual rate'], index_col = None, skiprows = 10, parse_cols = [0,29])
	df2 = pd.read_excel(file2, sheetname = 0, names = ['date','semi-anual rate'], index_col = None, skiprows = 10, parse_cols = [0,8])
	df3 = pd.read_excel(file2, sheetname = 1, names = ['date','semi-anual rate'], index_col = None, skiprows = 10, parse_cols = [0,2])

	# combine df1, df2, and df3, cleaning NaN value records, and reset index
	df = pd.concat([df1, df2, df3])
	df = df.dropna(how = 'any')
	df = df.reset_index()
	del df['index']

	# calculate EAR from Semi-annual rate and add to dataframe with name 'effective annual rate'
	date_list,rate_list = zip(*df.values)
	ear_list = map(lambda x: EAR(x), rate_list)
	df['effective annual rate'] = ear_list

	# plot time series ear
	plt.plot(df['date'], df['effective annual rate'], 'b-')
	plt.locator_params(axis ='y', nbins = 10)
	plt.title('Effective Annual Rate')
	plt.xlabel('Date')
	plt.ylabel('EAR')

	plt.show()
