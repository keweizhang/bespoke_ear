Bespoke Coding Challenge

This project is for Bespoke Coding Challenge 2017. 
The main task is to calculate and chart the time series Effective Annual Rate (EAR) 
from a series of semi-annul rate data, which can be downloaded from Reserve Bank of Australia (RBA) Website. 
An API is also exposed which can return the Effective Annual Rate Series in JSON format.


Prerequisites

python 2.7


Installing

Install all project required libraries from requirements.txt
1. activate your virtualenv
2. run: $ pip install -r requirements.txt


Simple Demo for Using the API

from EAR_lib import dump_json_EAR

dump_json_EAR(filename = "your-file-name", sheetindex = "your-sheet-index", parse_cols = "your-targeted-column-index")


